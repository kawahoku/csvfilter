package filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class DateTab extends AbstractTab
{
	private TextField min;
	private TextField max;
	private Label label;
	private static final String sep = "[^0-9]";
	public static final String regex = "[0-9]{1,4}" +sep+ "[0-9]{1,2}" +sep+ "[0-9]{1,2}";
	public static final Pattern pattern = Pattern.compile(regex);
	
	public DateTab(String title)
	{
		super(title);
		check.selectedProperty().addListener((observable, oldval, newval) -> {
			min.setDisable(!newval);
			max.setDisable(!newval);
		});

		min = new TextField();
		min.setPromptText("yyyy/mm/dd");
		addRules(min, pattern);
		max = new TextField();
		max.setPromptText("yyyy/mm/dd");
		addRules(max, pattern);
		label = new Label("~");
		flow.getChildren().addAll(min, label, max);

		Platform.runLater(() -> {
			check.setSelected(false);
			min.setDisable(true);
			max.setDisable(true);
		});
	}
	
	public static boolean matches(String value)
	{
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}
	
	@Override
	public boolean isValid()
	{
		return min.getStyleClass().contains("valid")
				& max.getStyleClass().contains("valid");
	}
	
	public long dateToLong(String date)
	{
		String[] sp = date.split(sep);
		return Long.valueOf(sp[0]) * 10000
				+ Long.valueOf(sp[1]) * 100
				+ Long.valueOf(sp[2]);
	}
	
	@Override
	public void clear()
	{
		
	}

	@Override
	public boolean pass(String cell)
	{
		if( !check.isSelected() || !isValid() ) return true;
		boolean flag = false;
		if( matches(cell) )
		{
			long num = dateToLong(cell);
			long min = dateToLong(this.min.getText());
			long max = dateToLong(this.max.getText());
			if( min <= num && num <= max )
			{
				flag = true;
			}
		}
		return flag;
	}
}