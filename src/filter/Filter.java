package filter;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;

public class Filter extends Stage
{
	private FilterController ctrl;
	
	public Filter()
	{
		try
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Filter.fxml"));
			ctrl = new FilterController();
			ctrl.setStage(this);
			loader.setController(ctrl);
			Parent root = loader.load();
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("filter.css").toExternalForm());
			setScene(scene);
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}
	
	public FilterController getController()
	{
		return ctrl;
	}
}