package filter;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.stage.Stage;
import table.CustomColumn;
import table.CustomRow;
import table.Table;

public class FilterController
{
	@FXML
	private TabPane tabpane;
	@FXML
	private Button close;
	@FXML
	private Button apply;
	@FXML
	private Button apply_and_close;
	
	private Stage stage;
	
	private Table table;
	

	public void initialize()
	{
		close.setOnAction(e -> stage.close());
		apply.setOnAction(e -> apply());
		apply_and_close.setOnAction(e -> {apply(); stage.close();});
	}
	
	public void clear()
	{
		for( Tab tab : tabpane.getTabs() )
		{
			((AbstractTab)tab).clear();
		}
	}
	
	public TabPane getTabPane()
	{
		return tabpane;
	}
	
	public void loadFilter()
	{
		System.out.println("Fileter loading...");
		tabpane.getTabs().clear();
		for( TableColumn col : table.getColumns() )
		{
			CustomColumn ccol = (CustomColumn) col;
			Class cl = ccol.getCLASS();

			AbstractTab tab;
			if( cl == DateTab.class )
			{
				tab = new DateTab(ccol.getText());
			}
			else if( cl == DoubleTab.class )
			{
				tab = new DoubleTab(ccol.getText());
			}
			else if( cl == BooleanTab.class )
			{
				tab = new BooleanTab(ccol.getText());
			}
			else
			{
				tab = new StringTab(ccol.getText());
				((StringTab) tab).setItems(ccol.getSortedList());
			}
			tabpane.getTabs().add(tab);
		}
	}
	
	public void setTable(Table table)
	{
		this.table = table;
	}
	
	public void setStage(Stage stage)
	{
		this.stage = stage;
	}
	
	public void apply()
	{
		ObservableList<CustomRow> list = FXCollections.observableArrayList();
		for( CustomRow row : table.getRowList() )
		{
			if( pass(row) )
			{
				list.add(row);
			}
		}
		table.setItems(list);
	}
	
	public boolean pass(CustomRow row)
	{
		boolean pass = true;
		for( int i=0; i<tabpane.getTabs().size(); i++ )
		{
			AbstractTab tab  = (AbstractTab) tabpane.getTabs().get(i);
			String val = row.getCells()[i];
			pass &= tab.pass(val);
		}
		return pass;
	}
}
