package filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.application.Platform;
import javafx.scene.control.CheckBox;

public class BooleanTab extends AbstractTab
{
	public static final String regex = "true|false";
	public static final Pattern pattern = Pattern.compile(regex);
	private CheckBox True;
	private CheckBox False;
	
	public BooleanTab(String title)
	{
		super(title);
		check.selectedProperty().addListener((observable, oldval, newval) -> {
			True.setDisable(!newval);
			False.setDisable(!newval);
		});

		True = new CheckBox("True");
		False = new CheckBox("False");
		flow.getChildren().addAll(True, False);
		Platform.runLater(() -> {
			check.setSelected(false);
			True.setDisable(true);
			False.setDisable(true);
		});
	}
	
	
	public static boolean matches(String value)
	{
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}
	
	@Override
	public void clear()
	{
		
	}

	@Override
	public boolean isValid()
	{
		return true;
	}
	
	@Override
	public boolean pass(String cell)
	{
		if( !check.isSelected() || !isValid() ) return true;
		boolean flag = false;
		String bool = cell.toLowerCase();
		if( True.isSelected()  ) flag |= bool.matches("false");
		if( False.isSelected() ) flag |= bool.matches("true");
		return flag;
	}
}
