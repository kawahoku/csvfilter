package filter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class StringTab extends AbstractTab
{
	private TextField autofield;
	private ListView<CheckBox> listview;
	private ObservableList<CheckBox> checklist;
	public static final String regex = ".*";
	public static final Pattern pattern = Pattern.compile(regex);
	
	private String keystring;
	private ExecutorService exec = Executors.newSingleThreadExecutor(r -> {
		Thread t = new Thread(r);
		t.setDaemon(true);
		return t;
	});
	
	
	public StringTab(String title)
	{
		super(title);
		check.selectedProperty().addListener((observable, oldval, newval) -> {
			listview.setDisable(!newval);
			autofield.setDisable(!newval);
		});

		listview = new ListView<>();
		border.setCenter(listview);
		
		autofield = new TextField();
		autofield.textProperty().addListener((ob, oldval, newval) -> {
			keystring = newval;
			exec.execute(autoCompleteTask(newval));
		});
		border.setBottom(autofield);

		Platform.runLater(() -> {
			check.setSelected(false);
			listview.setDisable(true);
			autofield.setDisable(true);
		});
	}
	
	public Task<Void> autoCompleteTask(String keyword)
	{
		return new Task<Void>() {
			@Override
			public Void call()
			{
				final ObservableList<CheckBox> newlist = FXCollections.observableArrayList();
				if( keyword.isEmpty() )
				{
					newlist.addAll(checklist);
				}
				else
				{
					String[] keys = keyword.toLowerCase().split("\\s+");
					for( CheckBox box : checklist )
					{
						if( !keyword.matches(keystring) ) return null;
						String text = box.getText().toLowerCase();
						for( String key : keys )
						{
							if( text.contains(key) )
							{
								newlist.add(box);
								break;
							}
						}
					}
				}
				Platform.runLater(() -> {
					listview.setItems(newlist);
				});
				return null;
			}
		};
	}
	
	public List<String> getSelected()
	{
		List<String> list = new ArrayList<>();
		for( CheckBox box : checklist )
		{
			if( box.isSelected() )
			{
				list.add(box.getText());
			}
		}
		return list;
	}
	
	public void setItems(List<String> colitems)
	{
		checklist = FXCollections.observableArrayList();
		for( String cell : colitems )
		{
			checklist.add(new CheckBox(cell));
		}
		listview.setItems(checklist);
	}
	
	public static boolean matches(String value)
	{
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}
	
	@Override
	public boolean isValid()
	{
		return true;
	}
	
	@Override
	public void clear()
	{
		checklist.clear();
		listview.setItems(null);
	}
	
	@Override
	public boolean pass(String cell)
	{
		if( !check.isSelected() || !isValid() ) return true;
		return getSelected().contains(cell);
	}
}
