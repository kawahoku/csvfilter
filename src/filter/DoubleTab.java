package filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class DoubleTab extends AbstractTab
{
	private TextField min;
	private TextField max;
	private Label label;
	public static final String regex = "[+-]?[0-9]+\\.?[0-9]*";
	public static final Pattern pattern = Pattern.compile(regex);
	
	public DoubleTab(String title)
	{
		super(title);
		check.selectedProperty().addListener((observable, oldval, newval) -> {
			min.setDisable(!newval);
			max.setDisable(!newval);
		});

		min = new TextField();
		addRules(min, pattern);
		max = new TextField();
		addRules(max, pattern);
		label = new Label("~");
		flow.getChildren().addAll(min, label, max);

		Platform.runLater(() -> {
			check.setSelected(false);
			min.setDisable(true);
			max.setDisable(true);
		});
	}
	
	public static boolean matches(String value)
	{
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}
	
	@Override
	public boolean isValid()
	{
		return min.getStyleClass().contains("valid")
				& max.getStyleClass().contains("valid");
	}
	
	@Override
	public void clear()
	{
		
	}
	
	@Override
	public boolean pass(String cell)
	{
		if( !check.isSelected() || !isValid() ) return true;
		boolean flag = false;
		if( matches(cell) )
		{
			Double num = Double.valueOf(cell);
			Double min = Double.valueOf(this.min.getText());
			Double max = Double.valueOf(this.max.getText());
			if( min <= num && num <= max )
			{
				flag = true;
			}
		}
		return flag;
	}
}
