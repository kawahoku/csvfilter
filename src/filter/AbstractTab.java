package filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;

public abstract class AbstractTab extends Tab
{
	protected BorderPane border;
	protected FlowPane flow;
	protected CheckBox check;

	
	public AbstractTab(String title)
	{
		super(title);
		border = new BorderPane();
		border.setPadding(new Insets(10));
		setContent(border);

		check = new CheckBox("Filter");
		border.setTop(check);
		
		flow = new FlowPane();
		flow.setPadding(new Insets(10));
		border.setCenter(flow);
	}
	
	public void addRules(TextField field, Pattern pattern)
	{
		field.getStyleClass().add("textfield");
		field.textProperty().addListener((ob, oldval, newval) -> {
			Matcher matcher = pattern.matcher(newval);
			if( matcher.matches() )
			{
				field.getStyleClass().add("valid");
			}
			else
			{
				field.getStyleClass().removeAll("valid");
			}
		});
	}
	
	public abstract void clear();

	public abstract boolean isValid();

	public abstract boolean pass(String cell);
}
