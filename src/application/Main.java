package application;
	
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


public class Main extends Application
{
	@Override
	public void start(Stage primaryStage)
	{
		try
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Main.fxml"));
			MainController ctrl = new MainController();
			loader.setController(ctrl);
			Pane root = loader.load();
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			
			primaryStage.setOnCloseRequest(e -> {
				Platform.exit();
				System.exit(0);
			});

			scene.getAccelerators().put(
					new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN),
					() -> ctrl.save()
			);
			scene.getAccelerators().put(
					new KeyCodeCombination(KeyCode.F, KeyCombination.CONTROL_DOWN),
					() -> ctrl.filtering()
			);
			scene.getAccelerators().put(
					new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN),
					() -> ctrl.load()
			);
			primaryStage.show();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
