package application;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import filter.Filter;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import table.Table;

public class MainController
{
	@FXML
	private BorderPane tablepane;
	@FXML
	private Button browse;
	@FXML
	private TextField textfield;
	@FXML
	private MenuItem filtermenu;
	@FXML
	private MenuItem savemenu;
	@FXML
	private MenuItem openmenu;
	
	private Table table;
	
	private Filter filter;

	private FileChooser chooser;
	
	private Stage stage;
	
	private ExecutorService exec;

	
	public void initialize()
	{
		table = new Table();
		filter = new Filter();
		filter.getController().setTable(table);

		tablepane.setCenter(table);

		chooser = new FileChooser();
		chooser.setInitialDirectory(new File(System.getProperty("user.home")));
		ExtensionFilter extensionfilter = new ExtensionFilter("CSV files (*.csv)",  "*.csv");
		chooser.getExtensionFilters().add(extensionfilter);

		browse.setOnAction(e -> load());
		openmenu.setOnAction(e -> load());
		savemenu.setOnAction(e -> save()); 
		filtermenu.setOnAction(e -> filtering()); 
	}

	public void clear()
	{
		table.clear();
		filter.getController().getTabPane().getTabs().clear();
		System.gc();
	}
	
	public void load()
	{
		File src = browse();
		if( src != null )
		{
			clear();
			exec = Executors.newSingleThreadExecutor(r -> {
				Thread t = new Thread(r);
				t.setDaemon(true);
				return t;
			});
			exec.execute(() -> {
				Platform.runLater(() -> {
					chooser.setInitialDirectory(src.getParentFile());
					textfield.setText(src.toString());
					table.setCSV(src);
					System.out.println("\ncsv set");
					filter.getController().loadFilter();
					System.out.println("\nfilter set");
				});
			});
		}
	}

	public void setStage(Stage stage)
	{
		this.stage = stage;
	}
	
	public File browse()
	{
		return chooser.showOpenDialog(null);
	}
	
	public void filtering()
	{
		filter.show();
	}
	
	public void save()
	{
		File file = chooser.showSaveDialog(null);

		if( file != null )
		{
			if( !file.getName().contains(".") )
			{
				file = new File(file.toString() + ".csv");
			}
			if( !file.getName().endsWith(".csv") )
			{
				error("invalid file name: " + file.getName() + "\n"
					+ "file name MUST end with `.csv`");
				return;
			}
			if( file.exists() )
			{
				String text = file + " already exists.\n"
							+ "Do you Overwrite it?";
				if( !confirm(text) )
				{
					return;
				}
			}
			System.out.println(file);
			String[] lines = table.toCSV();

			try( BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)))
			{
				for( String line : lines )
				{
					writer.write(line);
				}
				writer.flush();
				writer.close();
			}
			catch( IOException e )
			{
				e.printStackTrace();
			}
		}
	}
	
	public boolean confirm(String message)
	{
		Alert alert = new Alert(AlertType.CONFIRMATION, message, ButtonType.CANCEL, ButtonType.OK);
		alert.showAndWait();
		return (alert.getResult() == ButtonType.OK);
	}
	
	public boolean error(String message)
	{
		Alert alert = new Alert(AlertType.ERROR, message, ButtonType.OK);
		alert.showAndWait();
		return (alert.getResult() == ButtonType.OK);
	}
}
