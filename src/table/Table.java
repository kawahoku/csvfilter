package table;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class Table extends TableView<CustomRow>
{
	private File csv;
	private List<String> header;
	private ObservableList<CustomRow> rowlist;
	
	public Table()
	{
		rowlist = FXCollections.observableArrayList();
	}
	
	public void clear()
	{
		rowlist.clear();
		setItems(null);
		getColumns().clear();
	}
	
	public void setCSV(File csv)
	{
		this.csv = csv;
		loadCSV();
	}
	
	public CSVParser getParser(File csv)
	{
		try
		{
			BufferedReader reader = getReader(csv);
			CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT
					.withFirstRecordAsHeader()
					.withAllowMissingColumnNames()
					.withIgnoreHeaderCase()
					.withTrim()
					);
			return parser;
		}
		catch( IOException e )
		{
			error("invalid file: " + csv.toString() + "\n"
				+ "first row must NOT contain empty cell.");
			e.printStackTrace();
		}
		return null;
	}

	public BufferedReader getReader(File file)
	{
		try
		{
			BufferedReader reader = Files.newBufferedReader(csv.toPath());
			return reader;
		}
		catch( IOException e )
		{
			error(file + " cannot be opened.");
			e.printStackTrace();
		}
		return null;
	}
	
	public void loadCSV()
	{
		CSVParser parser = getParser(csv);

		// set header 
		header = parser.getHeaderNames();

		getColumns().clear();
		for( int i=0; i<header.size(); i++ )
		{
			String key = header.get(i);
			CustomColumn col = new CustomColumn(i, key);
			getColumns().add(col);
		}
		
		// set rows
		rowlist.clear();
		for( CSVRecord record : parser )
		{
			CustomRow row = new CustomRow(header.size());
			for( int i=0; i<record.size(); i++ )
			{
				String cell = record.get(i);
				row.getCells()[i] = cell;
			}
			rowlist.add(row);
		}

		setItems(rowlist);

		System.out.println(header);
		
		// set Columns' Class
		for( TableColumn col : getColumns() )
		{
			CustomColumn ccol = (CustomColumn) col;
			ccol.setCLASS();
		}
	}
	

	public String[] toCSV()
	{
		String[] lines = new String[getItems().size() + 1];
		int i = 0;
		
		lines[0] = toLine(header.toArray(new String[0]));
		
		for( CustomRow row : getItems() )
		{
			lines[++i] = toLine(row.getCells());
		}
		return lines;
	}
	
	public String toLine(String[] array)
	{
		String line = "";
		for( int j=0; j<array.length; j++ )
		{
			if( array[j].contains(",") )
			{
				line += "\"" + array[j] + "\"";
			}
			else
			{
				line += array[j];
			}

			if( j != array.length-1 )
			{
				line += ",";
			}
			else
			{
				line += "\n";
			}
		}
		return line;
	}
	
	public void layoutChildren()
	{
		super.layoutChildren();
		System.out.println(getMB() + "[MB]");
	}

	public static float getMB()
	{
		return (float)( (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024*1024) );
	}
	
	public ObservableList<CustomRow> getRowList()
	{
		return rowlist;
	}
	
	public boolean error(String message)
	{
		Alert alert = new Alert(AlertType.ERROR, message, ButtonType.OK);
		alert.showAndWait();
		return (alert.getResult() == ButtonType.OK);
	}
}
