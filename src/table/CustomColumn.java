package table;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import filter.BooleanTab;
import filter.DateTab;
import filter.DoubleTab;
import filter.StringTab;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;

public class CustomColumn extends TableColumn<CustomRow, String>
{
	private Class CLASS;
	private Map<Class, Integer> ballot;
	private int nvoted;
	
	public CustomColumn(int idx, String key)
	{
		super(key);
		setCellValueFactory(cellDataFeatures -> {
			return new SimpleStringProperty(cellDataFeatures.getValue().getCells()[idx]);
		});
		ballot = new HashMap<>();
		nvoted = 0;
	}
	
	public ObservableList<String> getSortedList()
	{
		Set<String> set = new HashSet<>();
		int size = getTableView().getItems().size();
		for( int i=0; i<size; i++ )
		{
			set.add(getCellData(i));
		}

		ObservableList<String> list = FXCollections.observableArrayList(set);
		FXCollections.sort(list);
		return list;
	}
	
	public Class getCLASS()
	{
		return CLASS;
	}
	
	public void setCLASS(Class CLASS)
	{
		this.CLASS = CLASS;
	}
	
	public void setCLASS()
	{
		int size = getTableView().getItems().size();
		if( nvoted == 0 || nvoted != size )
		{
			ballot.clear();
			for( int i=0; i<size; i++ )
			{
				String cell = getCellData(i);
				vote(getCellClass(cell));
			}
		}

		int max = 0;
		for( Map.Entry<Class, Integer> e : ballot.entrySet() )
		{
			if( e.getKey() == Void.class ) continue;
			if( max < e.getValue() )
			{
				max = e.getValue();
				CLASS = e.getKey();
			}
		}
	}

	public Class getCellClass(String cell)
	{
		Class out = StringTab.class;
		if( cell.isEmpty() )  out = Void.class;
		cell = cell.toLowerCase();
		if( DoubleTab.matches(cell) )
		{
			out = DoubleTab.class;
		}
		else if( DateTab.matches(cell) )
		{
			out = DateTab.class;
		}
		else if( BooleanTab.matches(cell) )
		{
			out = BooleanTab.class;
		}
		return out;
	}
	
	public void vote(String cell)
	{
		vote(getCellClass(cell));
	}
	
	public void vote(Class c)
	{
		int count = ( ballot.containsKey(c) ? ballot.get(c) : 0 );
		ballot.put(c, count+1);
		nvoted++;
	}
}