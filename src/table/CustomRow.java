package table;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.TableRow;

public class CustomRow extends TableRow<String>
{
	private String[] cells;
	
	public CustomRow(int length)
	{
		cells = new String[length];
	}
	
	public String[] getCells()
	{
		return cells;
	}
}